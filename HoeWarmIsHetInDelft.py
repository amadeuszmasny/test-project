
from typing import List

import logging
import requests

logger = logging.getLogger('HoeWarmIsHetInDelft')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

URL = "https://weerindelft.nl/clientraw.txt"
TIMEOUT = 10
CURRENT_TEMP_INDEX = 4


class DataValidationError(ValueError):
    """Raised when downloaded data is not valid."""

    def __init__(self, error=None, message="Fetched data is not valid, cannot determine correct temperature."):
        self.message = message
        self.error = error
        super().__init__(f"{self.message} ERROR: {self.error}")


def data_validation(data: List[str]) -> None:
    """Data validation function based on original validation from js script.

    Parameters
    ----------
    data: List[str], required
        Fetched data from endpoint.
    """
    # check data length
    if len(data) < 5:
        raise DataValidationError(
            error=f"Data is too short. Temperature should be on the 5th place but data is {len(data)} long."
        )

    # now make sure we got the entire clientraw.txt
    # valid clientraw.txt has '12345' at start and '!!' at end of record
    if not (data[0] == '12345' and data[-1].endswith('!!')):
        raise DataValidationError(
            error=f"Valid data should have '12345' at the start and '!!' at the end of record."
        )

    # try cast temperature to int
    try:
        cast_to_int(data[CURRENT_TEMP_INDEX])

    except ValueError:
        raise DataValidationError(
            error=f"Valid temperature should be able to be cast to int. Temp value: {data[CURRENT_TEMP_INDEX]}"
        )


def cast_to_int(value: str) -> int:
    """Try cast to int string value."""
    return int(float(value))


def data_preparation(response: requests.Response) -> List[str]:
    """Data preparation function for further expansion.

    Parameters
    ----------
    response: requests.Response
        Response object from the site containing temperature data.

    Returns
    -------
    List with strings
    """
    data = response.text.strip().split(" ")
    return data


def get_current_temperature() -> int:
    """Retrieves current temperature in degrees (rounded) Celcius.

    Returns
    -------
    Int (current temperature in Celcius degrees)
    """
    response = requests.get(URL, timeout=TIMEOUT)
    logger.debug(f"Downloaded data: {response.text}")

    data = data_preparation(response=response)
    data_validation(data=data)

    current_temperature = cast_to_int(data[CURRENT_TEMP_INDEX])

    return current_temperature


if __name__ == "__main__":
    print(get_current_temperature())
