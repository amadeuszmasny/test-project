FROM python:3.6

WORKDIR /usr/src/app

# Install dependencies
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# Run temperature script
CMD ["python", "HoeWarmIsHetInDelft.py"]
